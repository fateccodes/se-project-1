### Segurança da Informação
####Daniel Pestana - ADS 5º Semestre

##Objetivo

O objetivo do projeto é dar acesso completo ao banco de dados pro desenvolvedor sem fornecer o usuário e senha.

##API

O princípio da API é fornecer uma interface entre o frontend e o backend para não fornecer dados sensíveis e senha do banco.

Motivos pela escolha da API:
- A API é entendida pela maioria das linguagens.
- Frontend separado do Backend.
- Alguns recursos de segurança oferecidos pelo framework.
- Requisições a partir de token (não necessita login e senha; Backend
consegue expirar esse token quando quiser).

## Tecnologia e Framework

A linguagem utilizada neste projeto é o PHP por oferecer um framework extremamente útil chamado Laravel (há outros que tem como base a mesma ideia como o Slim).

Um dos motivos pela escolha deste framework foi pela sua segurança, ja que ao criar uma senha por exemplo, ele nunca irá salvar no formato texto e tem um algoritmo próprio para a criação de senha criptografadas.

######Empresas que usam Laravel no Brasil
######https://github.com/especializati/empresas-que-usam-laravel-no-brasil

##Interface - Insomnia

Para melhor vizualização da interface, foi utilizado um programa chamado Insomnia, que auxilia o desenvolvedor 